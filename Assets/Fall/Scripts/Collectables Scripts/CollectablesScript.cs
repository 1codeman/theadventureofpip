﻿using UnityEngine;
using System.Collections;

public class CollectablesScript : MonoBehaviour {

	void OnEnable() {
        Invoke("DestroyCollectables",6f);
    }

  
    void DestroyCollectables() {
        gameObject.SetActive(false);
    }
}
