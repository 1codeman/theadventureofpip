﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;


public class MainMenuController : MonoBehaviour
{

    [SerializeField]
    private Button musicBtn;

    [SerializeField]
    private Sprite[] musicIcons;
    // Use this for initialization
    void Start()
    {
        CheckToPlayTheMusic();
    }

    void CheckToPlayTheMusic()
    {
        if (GamePreferences.GetMusicState() == 1)
        {
            MusicController.instance.PlayMusic(true);
            musicBtn.image.sprite = musicIcons[1];
        }
        else if (GamePreferences.GetMusicState() == 0)
        {
            MusicController.instance.PlayMusic(false);
            musicBtn.image.sprite = musicIcons[0];
        }
    }

    public void StartGame()
    {
        //Application.LoadLevel("Gameplay");
        GameManagerController.instance.gameStartedFromMainMenu = true;
        //SceneManager.LoadScene("Gameplay
        SceneFader.instance.LoadLevel("Gameplay");
     
    }

    public void HighscoreMenu()
    {
        //Application.LoadLevel("Gameplay");
        SceneManager.LoadScene("HighscoreScene");
    }

    public void OptionsMenu()
    {
        //Application.LoadLevel("Gameplay");
        SceneManager.LoadScene("OptionsMenu");
    }
    public void RootMenu()
    {
        //Application.LoadLevel("Gameplay");
        SceneManager.LoadScene("Root");
    }

    public void QuitGame()
    {
        //Application.LoadLevel("Gameplay");
        Application.Quit();
    }

    public void MusicButton()
    {
        if (GamePreferences.GetMusicState() == 0)
        {
            GamePreferences.SetMusicState(1);
            MusicController.instance.PlayMusic(true);
            musicBtn.image.sprite = musicIcons[1];
        }
        else if (GamePreferences.GetMusicState() == 1)
        {
            GamePreferences.SetMusicState(0);
            MusicController.instance.PlayMusic(false);
            musicBtn.image.sprite = musicIcons[0];
        }
    }

}//MainMenuController


