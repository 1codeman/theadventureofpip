﻿using UnityEngine;
using System.Collections;

public static class GamePreferences {

    public static string EasyDifficulty = "EasyDifficulty";
    public static string MediumDifficulty = "MediumDifficulty";
    public static string HardDifficulty = "HardDifficulty";

    public static string EasyDifficultyHighScore = "EasyDifficultyHighScore";
    public static string MediumDifficultyHighScore = "MediumDifficultyHighScore";
    public static string HardDifficultyHighScore = "HardDifficultyHighScore";


    public static string EasyDifficultyCoinScore = "EasyDifficultyCoinScore";
    public static string MediumDifficultyCoinScore = "MediumDifficultyCoinScore";
    public static string HardDifficultyCoinScore = "HardDifficultyCoinScore";

    public static string IsMusicOn = "IsMusicOn";

    //NOTE we are going to use integer to represent bool variables
    //0 = false, 1 = true

    public static int GetMusicState() {
        return PlayerPrefs.GetInt(GamePreferences.IsMusicOn);
    }

    // 0 is off, 1 is on
     public static void SetMusicState(int state) {
        PlayerPrefs.SetInt(GamePreferences.IsMusicOn, state);
    }

    //method for getting easy difficulty state
    public static int GetEasyDifficulty() {
        return PlayerPrefs.GetInt(GamePreferences.EasyDifficulty);
    }

    //method for setting easy difficulty state
    public static void SetEasyDifficulty(int difficulty) {
        PlayerPrefs.SetInt(GamePreferences.EasyDifficulty, difficulty);
    }

    
    //method for getting medium difficulty state
    public static int GetMediumDifficulty() {
        return PlayerPrefs.GetInt(GamePreferences.MediumDifficulty);
    }

    //method for setting medium difficulty state
    public static void SetMediumDifficulty(int difficulty) {
        PlayerPrefs.SetInt(GamePreferences.MediumDifficulty, difficulty);
    }

     //method for getting hard difficulty state
    public static int GetHardDifficulty() {
        return PlayerPrefs.GetInt(GamePreferences.HardDifficulty);
    }

    //method for setting hard difficulty state
    public static void SetHardDifficulty(int difficulty) {
        PlayerPrefs.SetInt(GamePreferences.HardDifficulty, difficulty);
    }

    //method for getting easy difficulty highscore
    public static int GetEasyDifficultyHighscore() {
        return PlayerPrefs.GetInt(GamePreferences.EasyDifficultyHighScore);
    }

    //method for setting easy difficulty highscore
    public static void SetEasyDifficultyHighScore(int score) {
        PlayerPrefs.SetInt(GamePreferences.EasyDifficultyHighScore, score);
    }

    //method for getting medium difficulty highscore
    public static int GetMediumDifficultyHighscore() {
        return PlayerPrefs.GetInt(GamePreferences.MediumDifficultyHighScore);
    }

    //method for setting medium difficulty highscore
    public static void SetMediumDifficultyHighScore(int score) {
        PlayerPrefs.SetInt(GamePreferences.MediumDifficultyHighScore, score);
    }

     //method for getting hard difficulty highscore
    public static int GetHardDifficultyHighscore() {
        return PlayerPrefs.GetInt(GamePreferences.HardDifficultyHighScore);
    }

    //method for setting hard difficulty highscore
    public static void SetHardDifficultyHighScore(int score) {
        PlayerPrefs.SetInt(GamePreferences.HardDifficultyHighScore, score);
    }

     //method for getting easy difficulty CoinScore
    public static int GetEasyDifficultyCoinScore() {
        return PlayerPrefs.GetInt(GamePreferences.EasyDifficultyCoinScore);
    }

    //method for setting easy difficulty CoinScore
    public static void SetEasyDifficultyCoinScore(int score) {
        PlayerPrefs.SetInt(GamePreferences.EasyDifficultyCoinScore, score);
    }

    //method for getting medium difficulty CoinScore
    public static int GetMediumDifficultyCoinScore() {
        return PlayerPrefs.GetInt(GamePreferences.MediumDifficultyCoinScore);
    }

    //method for setting medium difficulty highscore
    public static void SetMediumDifficultyCoinScore(int score) {
        PlayerPrefs.SetInt(GamePreferences.MediumDifficultyCoinScore, score);
    }

    //method for getting hard difficulty CoinScore
    public static int GetHardDifficultyCoinScore() {
        return PlayerPrefs.GetInt(GamePreferences.HardDifficultyCoinScore);
    }

    //method for setting hard difficulty CoinScore
    public static void SetHardDifficultyCoinScore(int score) {
        PlayerPrefs.SetInt(GamePreferences.HardDifficultyCoinScore, score);
    }







}//GamePreferances
