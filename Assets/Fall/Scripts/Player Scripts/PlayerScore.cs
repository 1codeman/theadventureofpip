﻿using UnityEngine;
using System.Collections;

public class PlayerScore : MonoBehaviour {

    [SerializeField]
    private AudioClip coinClip, lifeClip;

    private CameraScript cameraScript;
    private Vector3 previousPosition;
    private bool countScore;

    public static int scoreCount;
    public static int tempCount;
    public static int lifeCount;
    public static int coinCount;

    void Awake() {
        cameraScript = Camera.main.GetComponent<CameraScript>();

    }
	// Use this for initialization
	void Start () {
        previousPosition = transform.position;
        countScore = true;
        GameplayController.instance.SetScore(scoreCount);
        GameplayController.instance.SetCoinScore(coinCount);
        GameplayController.instance.SetLifeScore(lifeCount);

    }
    // Update is called once per frame
	void Update () {
        CountScore();
    }
    void CountScore() {
        if (countScore) {
            if (transform.position.y < previousPosition.y )
            {
                tempCount++;
                if (tempCount == 20)
                {
                    scoreCount++;
                    tempCount = 0;

                }

            }

            previousPosition = transform.position;
            GameplayController.instance.SetScore(scoreCount);
        }   

    }
	
    void OnTriggerEnter2D(Collider2D target) {
        if(target.tag == "Coin") {
            coinCount++;
            scoreCount += 1;

            GameplayController.instance.SetScore(scoreCount);
            GameplayController.instance.SetCoinScore(coinCount);

            AudioSource.PlayClipAtPoint(coinClip, transform.position);
            target.gameObject.SetActive(false);
        }    
        if(target.tag == "Life") {
            lifeCount++;
            scoreCount += 2;

            GameplayController.instance.SetScore(scoreCount);
            GameplayController.instance.SetLifeScore(lifeCount);

            AudioSource.PlayClipAtPoint(lifeClip, transform.position);
            target.gameObject.SetActive(false);
        }
        if(target.tag == "Bounds" || target.tag == "Deadly") {
            cameraScript.moveCamera = false;
            countScore = false;

            transform.position = new Vector3(500, 500, 0);
            lifeCount--;
            GameManagerController.instance.CheckGameStatus(scoreCount, coinCount, lifeCount);
        }     
       
    }
	
}
