﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RootController : MonoBehaviour {

    public void TapMenu()
    {
        //Application.LoadLevel("Gameplay");
        SceneManager.LoadScene("Tap_MainMenu");
    }

    public void FallMenu()
    {
        //Application.LoadLevel("Gameplay");
        SceneManager.LoadScene("MainMenu");
    }

    public void PuzzleMenu()
    {
        //Application.LoadLevel("Gameplay");
        SceneManager.LoadScene("IQ_Gameplay");
    }

    public void QuitGame()
    {
        //Application.LoadLevel("Gameplay");
        Application.Quit();
    }
}
