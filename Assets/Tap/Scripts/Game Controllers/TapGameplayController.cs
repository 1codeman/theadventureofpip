﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TapGameplayController : MonoBehaviour {

	public static TapGameplayController instance;
	
	[SerializeField]
	private Text scoreText, endScore, bestScore, gameOverText;
	
	[SerializeField]
	private Button restartGameButton, instructionsButton;
	
	[SerializeField]
	private GameObject pausePanel;
	
	[SerializeField]
	private GameObject[] birds;
	
	[SerializeField]
	private Sprite[] medals;
	
	[SerializeField]
	private Image medalImage;

	void Awake() {
		MakeInstance ();
		Time.timeScale = 0f;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	void MakeInstance() {
		if (instance == null) {
			instance = this;
		}
	}

	public void PauseGame() {
		if (BirdScript.instance != null) {
			if(BirdScript.instance.isAlive) {
				pausePanel.SetActive(true);
				gameOverText.gameObject.SetActive(false);
				endScore.text = "" + BirdScript.instance.score;
				bestScore.text = "" + TapGameController.instance.GetHighscore();
				Time.timeScale = 0f;
				restartGameButton.onClick.RemoveAllListeners();
				restartGameButton.onClick.AddListener(() => ResumeGame());
			}
		}
	}

	public void GoToMenuButton() {
		TapSceneFader.instance.FadeIn ("Tap_MainMenu");
	}

	public void ResumeGame() {
		pausePanel.SetActive (false);
		Time.timeScale = 1f;
	}

	public void RestartGame() {
		TapSceneFader.instance.FadeIn (Application.loadedLevelName);
	}

	public void PlayGame() {
		scoreText.gameObject.SetActive (true);
		birds [TapGameController.instance.GetSelectedBird ()].SetActive (true);
		instructionsButton.gameObject.SetActive (false);
		Time.timeScale = 1f;
	}

	public void SetScore(int score) {
		scoreText.text = "" + score;
	}

	public void PlayerDiedShowScore(int score) {

        BirdScript.instance.isAlive = false;

        pausePanel.SetActive (true);
		gameOverText.gameObject.SetActive (true);
		scoreText.gameObject.SetActive (false);

		endScore.text = "" + score;

		if (score > TapGameController.instance.GetHighscore ()) {
			TapGameController.instance.SetHighscore(score);
		}

		bestScore.text = "" + TapGameController.instance.GetHighscore ();

		if (score <= 20) {
			medalImage.sprite = medals [0];
		} else if (score > 20 && score < 40) {
			medalImage.sprite = medals [1];

			if (TapGameController.instance.IsGreenBirdUnlocked () == 0) {
				TapGameController.instance.UnlockGreenBird ();
			}

		} else {
			medalImage.sprite = medals [2];
			
			if (TapGameController.instance.IsGreenBirdUnlocked () == 0) {
				TapGameController.instance.UnlockGreenBird ();
			}

			if (TapGameController.instance.IsRedBirdUnlocked () == 0) {
				TapGameController.instance.UnlockRedBird ();
			}

		}

		restartGameButton.onClick.RemoveAllListeners ();
		restartGameButton.onClick.AddListener (() => RestartGame());
         
    }

}
