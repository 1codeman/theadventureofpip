﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class TapMenuController : MonoBehaviour {
	public static TapMenuController instance;

	[SerializeField]
	private GameObject[] birds;

	private bool isGreenBirdUnlocked, isRedBirdUnlocked;

	[SerializeField]
	private Animator notificationAnim;
	
	[SerializeField]
	private Text notificationText;
		
	void Awake() {
		MakeInstance();

	}

	void Start() {
		birds [TapGameController.instance.GetSelectedBird ()].SetActive (true);
		CheckIfBirdsAreUnlocked ();
	}

	void MakeInstance() {
		if (instance == null) {
			instance = this;
		}
	}

	void CheckIfBirdsAreUnlocked() {
		if (TapGameController.instance.IsRedBirdUnlocked () == 1) {
			isRedBirdUnlocked = true;
		}

		if (TapGameController.instance.IsGreenBirdUnlocked () == 1) {
			isGreenBirdUnlocked = true;
		}
	}

	public void PlayGame() {
		TapSceneFader.instance.FadeIn ("Tap_Gameplay");
	}

	public void ConnectOnGooglePlayGames() {
		//LeaderboardsController.instance.ConnectOrDisconnectOnGooglePlayGames ();
	}

	public void OpenLeaderboardsScoreUI() {
		//LeaderboardsController.instance.OpenLeaderboardsScore ();
	}

	public void ConnectOnTwitter() {
		//SocialMediaController.instance.LogInOrLogOutTwitter ();
	}

	public void ShareOnTwitter() {
		//SocialMediaController.instance.ShareOnTwitter ();
	}

    public void RootMenu()
    {
        //Application.LoadLevel("Gameplay");
        SceneManager.LoadScene("Root");
    }


    public void ChangeBird() {

		if (TapGameController.instance.GetSelectedBird () == 0) {

			if (isGreenBirdUnlocked) {
				birds [0].SetActive (false);
				TapGameController.instance.SetSelectedBird (1);
				birds [TapGameController.instance.GetSelectedBird ()].SetActive (true);
			}

		} else if (TapGameController.instance.GetSelectedBird () == 1) {

			if (isRedBirdUnlocked) {

				birds [1].SetActive (false);
				TapGameController.instance.SetSelectedBird (2);
				birds [TapGameController.instance.GetSelectedBird ()].SetActive (true);

			} else {

				birds [1].SetActive (false);
				TapGameController.instance.SetSelectedBird (0);
				birds [TapGameController.instance.GetSelectedBird ()].SetActive (true);

			}

		} else if (TapGameController.instance.GetSelectedBird () == 2) {
			birds [2].SetActive (false);
			TapGameController.instance.SetSelectedBird (0);
			birds [TapGameController.instance.GetSelectedBird ()].SetActive (true);
		}

	}

	public void NotificationMessage(string message) {
		StartCoroutine (AnimateNotificationPanel(message));
	}

	IEnumerator AnimateNotificationPanel(string message) {
		notificationAnim.Play("SlideIn");
		notificationText.text = message;
		yield return StartCoroutine(MyCoroutine.WaitForRealSeconds(2f));
		notificationAnim.Play("SlideOut");
	}

}
